class Desconto

  def initialize(valor, qtde)
    @valor = valor
    @qtde = qtde
  end

  def calc_desc(qtde)
    (qtde / @qtde) * @valor
  end

end

class Preco
	def initialize(preco_unit, *desc)
		@preco_unit = preco_unit
		@desc = desc
	end

	def calc_preco(qtde)
		(@preco_unit * qtde) - @desc
	end
end

RULES = {
  'A' => Preco.new(50, Desconto.new(20, 3)),
  'B' => Preco.new(30, Desconto.new(15, 2)),
  'C' => Preco.new(20),
  'D' => Preco.new(15),
}

class CheckOut

  def initialize(regras)
    @regras = regras
    @items = Hash.new
  end

  def scan(item)
    @items[item] ||= 0
    @items[item] += 1
  end

  def preco_por(item, qtde)
    if regras_por(item)
      regras_por(item).preco_por(qtde)
    else
      raise "Item invalido '#{item}'"
    end
  end

  def regras_por(item)
    @regras[item]
  end
end